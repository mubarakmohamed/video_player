// c'est ici que ça se passe
console.log('initialisation des contrôles du lecteur vidéo');
var vid = document.getElementById("balise_video"); 
document.getElementById("bouton_jouer").onclick = function () {
vid.play();
}

document.getElementById("bouton_pause").onclick = function () {
vid.pause();
}

document.getElementById("bouton_stop").onclick = function () {
vid.load();
}
document.getElementById("bouton_vol_plus").onclick = function () {
vid.volume += 0.2;
}
document.getElementById("bouton_vol_moins").onclick = function () {
vid.volume -= 0.2;
}
document.getElementById("bouton_fullscreen").onclick = function () {
if (vid.requestFullscreen) {
  vid.requestFullscreen();
} else if (vid.mozRequestFullScreen) {
  vid.mozRequestFullScreen();
} else if (vid.webkitRequestFullscreen) {
  vid.webkitRequestFullscreen();
} else if (vid.msRequestFullscreen) { 
  vid.msRequestFullscreen();
}
}