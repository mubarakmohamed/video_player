# Video Player

Lecteur vidéo

Ce lecteur vidéo est en panne, les consignes ne répondent plus...
Le mécano HTML/CSS est en RTT. Seul le dev JS est d'attaque.

## Consignes

Les fonctions classiques d'un lecteur vidéo doivent être implémentée en JS

- Lancer la lecture

- Mettre en pause

- Arrêter la lecture

- Monter et baisser le volume

- Mettre en plein écran

- _Option 1 : afficher le temps de lecture en cours dans le titre de la page (dans l'onglet)_

- _Option 2 : ajouter un bouton pour télécharger la vidéo_

- _Option 3, prendre une capture écran de l'image de la vidéo en cours (et l'afficher sur la page)_

### Ressources

Le HTML est fourni, il ne faut pas le toucher (sauf pour les options 2 et 3), seul le fichier JS doit être utilisé / modifié.
